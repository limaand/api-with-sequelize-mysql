const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')


const ProductController = require('./controllers/ProductController')
const OrderController = require('./controllers/OrderController')
const TransactionController = require('./controllers/TransactionController')
const UserController = require('./controllers/UserController')

router.get('/products', ProductController.index)
router.post('/product', ProductController.store)

router.get('/orders', OrderController.index)
router.post('/order', OrderController.store)
router.patch('/order/:id', OrderController.update)
router.delete('/order/:id', OrderController.delete)

router.post('/transaction', TransactionController.store)
router.get('/transaction/:id', TransactionController.show)



router.get('/users', verifyJWT, UserController.index)

/*router.get('/users', verifyJWT, (req, res, next) => {
    UserController.index(req, res, next);
})*/

//router.get('/users',  UserController.index)


router.post('/user', UserController.store)
router.post('/login', UserController.login)
router.get('/logout', UserController.logout)




function verifyJWT(req, res, next) {
    var token = req.headers['x-token'];

    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

    jwt.verify(token, process.env.SECRET, function(err, decoded, auth) {
        if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });

        // se tudo estiver ok, salva no request para uso posterior
        req.userId = decoded.id;
        next();
    });
}




module.exports = router