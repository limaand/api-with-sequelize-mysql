const models = require('../../models')
const jwt = require('jsonwebtoken')
const secret = process.env.SECRET

exports.index = (req, res) => {

    models.User.findAll()
        .then((results) => {

            res.json(

                results
            )


        })





}

exports.store = (req, res, next) => {
    models.User.create(req.body)
        .then((results) => {
            res.json(results)
        }).catch((next) => {
            res.status(500).json({
                field: next.errors[0].path,
                message: next.errors[0].message
            })
        })
}

exports.login = (req, res, next) => {

    models.User.findOne({

        where: {
            email: req.body.email,
            password: req.body.password,
        },
        order: [
            ['createdAt', 'DESC']
        ],


    }).then((results) => {


        if (results != null) {

            var token = jwt.sign({ id: results.id }, process.env.SECRET, {
                expiresIn: 300 // expires in 5min
            });
            res.status(200).send({ auth: true, token: token });

        } else {
            res.json({
                results
            })

        }

        // res.json(results)
    }).catch((next) => {
        res.status(500).json({
            field: next.errors[0].path,
            message: next.errors[0].message
        })
    })

}







exports.logout = (req, res, next) => {

    res.status(200).send({ auth: false, token: null });

}